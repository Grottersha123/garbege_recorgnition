from os import makedirs


def create_dir(path):
    try:
        makedirs(path)
        print("Directory ", path, " Created ")
    except FileExistsError:
        print("Directory ", path, " already exists")
