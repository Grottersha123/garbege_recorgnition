import logging

logging.getLogger().setLevel(logging.INFO)

import numpy as np
from keras import backend as K
from keras.engine.saving import load_model
from keras.preprocessing import image


def f1(y_true, y_pred):
    def recall(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

    def precision(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

    precision = precision(y_true, y_pred)
    recall = recall(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))


class Classification:
    def __init__(self, model_path=r'', img_list=[], size=(800, 800), show=False, classes=True):
        self.model_path = model_path
        self.img_list = img_list
        self.model = None
        self.show = show
        self.size = size
        self.classes = classes

    def load(self):
        self.model = load_model(self.model_path, custom_objects={'f1': f1})

    def load_image(self, img_path):
        img = image.load_img(img_path, target_size=self.size)
        img_tensor = image.img_to_array(img)  # (height, width, channels)
        img_tensor = np.expand_dims(img_tensor,
                                    axis=0)  # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
        img_tensor /= 255.  # imshow expects values in the range [0, 1]
        if self.show:
            pass
            # plt.imshow(img_tensor[0])
            # plt.axis('off')
            # plt.show()
        return img_tensor

    # TODO: реализовать метод для проверки модели
    # def evulate_report(path, model, size, show=False):
    #     pred_classes = []
    #     true_classes = []
    #     count = 0
    #     for i in os.listdir(path):
    #         for j in os.listdir(os.path.join(path, i)):
    #             true_classes.append(int(i ) -1)
    #             res = predict_class_scrap(os.path.join(path, i, j), model, size=size)
    #             count += 1
    #             print( 'Загружено %s' % count)
    #             pred_classes.append(res[0])
    #     print(classification_report(pred_classes, true_classes))

    # TODO: Загрузка партии металлолома предсказывания для всей партии предсказания.

    def predict_class_scrap(self, img):
        img = self.load_image(img, size=self.size, show=self.show)
        if self.classes:
            predict = self.model.predict(img)
        else:
            predict = self.model.predict(img).argmax(axis=-1)
        K.clear_session()
        return predict

    # TODO: размер картинки не высчитывается для предсказания
    def predict_multiple_class_scrap(self):
        images = []
        for im in self.img_list:
            img = self.load_image(im)
            images.append(img)
        images = np.vstack(images)
        if self.classes:
            predict = np.around(self.model.predict(images), decimals=2) * 100
        K.clear_session()
        average = np.average(predict, axis=0)
        logging.info('len detects {}, class_predicted {}'.format(len(self.img_list), predict.argmax(axis=-1).tolist()))
        return {'img_pred': predict.tolist(), 'avg_pred': np.around(average, decimals=2).tolist(),
                'class_pred': predict.argmax(axis=-1).tolist()}
