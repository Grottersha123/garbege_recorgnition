import glob
import unittest

from detection_module import Detection

conf_detect_dict = dict(
    path_model=r'D:\Git_project\garbege_recorgnition\models\detection\models\detection_model-ex-021--loss-0005.981.h5',
    path_json=r'D:\Git_project\garbege_recorgnition\models\detection\json\detection_config.json',
    list_img=glob.glob(r"D:\Git_project\garbege_recorgnition\test\images\*")[:10])


class MyTestDetectopn(unittest.TestCase):
    def setUp(self):
        self.detect = Detection(**conf_detect_dict)

    def test_load_models(self):
        self.assertIsNone(self.detect.load())

    def test_recor(self):
        self.detect.load()
        self.assertRaises(Exception,
                          self.detect.custom_detection_yolo(path_out=r'D:\Git_project\garbege_recorgnition\test\out'))


if __name__ == '__main__':
    unittest.main()
