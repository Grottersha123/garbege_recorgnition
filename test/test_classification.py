import glob
import unittest
from os.path import join as p_join

from classification_module import Classification
from conf import ROOT_DIR

conf_dict = dict(model_path=p_join(ROOT_DIR, r'models\classification\model_92_l.h5'),
                 img_list=glob.glob(p_join(ROOT_DIR, 'test\images\*'))[20:31])


class MyTestCase(unittest.TestCase):
    def setUp(self):
        self.cls = Classification(**conf_dict)

    def test_load_models(self):
        self.assertRaises(Exception, self.cls.load())

    def test_load_img(self):
        self.assertIsNotNone(self.cls.load_image(self.cls.img_list[0]))

    def test_cls(self):
        self.cls.load()
        self.assertRaises(Exception, self.cls.predict_multiple_class_scrap())


if __name__ == '__main__':
    unittest.main()
