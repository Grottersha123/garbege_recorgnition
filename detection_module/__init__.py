import logging
import os

logging.getLogger().setLevel(logging.INFO)
import numpy as np
from imageai.Detection.Custom import CustomObjectDetection

from utils import create_dir

import cv2


class Detection:
    def __init__(self, path_model='', path_json='', list_img='', class_label=None):
        self.path_model = path_model
        self.path_json = path_json
        self.path_img = list_img
        self.detector = CustomObjectDetection()
        self.detections_box_yolo = []
        self.detections_box = []
        self.class_label = class_label

    @staticmethod
    def filter_bounding_box(coef):
        pass

    def load(self):
        self.detector.setModelTypeAsYOLOv3()
        self.detector.setModelPath(self.path_model)
        self.detector.setJsonPath(self.path_json)
        self.detector.loadModel()

    def test_out(self):
        pass

    def detection_yolo(self, minimum_percentage=80, nms_treshold=0.4, path_out=r''):
        create_dir(path_out)
        for im in self.path_img:
            im_name = os.path.basename(im)
            detections = self.detector.detectObjectsFromImage(input_image=im,
                                                              output_image_path="{}/{}".format(path_out, im_name),
                                                              minimum_percentage_probability=minimum_percentage,
                                                              nms_treshold=nms_treshold)

            logging.info('Your images in yolo save in path {}'.format(path_out))
            self.detections_box_yolo.append({"{}/{}".format(path_out, im_name): detections})

    def custom_detection_yolo(self, minimum_percentage=80, nms_treshold=0.4, percentage_probability=True,
                              filter_box=False, path_out=r''):
        create_dir(path_out)
        for im in self.path_img:
            im_name = os.path.basename(im)
            detections = self.detector.detectObjectsFromImage(input_image=im,
                                                              output_image_path="file",
                                                              output_type='array',
                                                              minimum_percentage_probability=minimum_percentage,
                                                              nms_treshold=nms_treshold)

            if filter_box:
                pass

            self.detections_box.append({'img_name': im_name, 'boxes': detections[-1]})
            logging.info('len detects {}'.format(len(detections[-1])))
            image = cv2.imread(im)
            for detect in detections[-1]:
                class_name, percent, rectangle_box = detect["name"], detect["percentage_probability"], detect[
                    "box_points"]
                if self.class_label:
                    class_name
                x1, y1, x2, y2 = rectangle_box
                label = "%s" % class_name
                if percentage_probability:
                    label = "%s : %.3f" % (class_name, percent)
                thickness = 2
                image = cv2.rectangle(image, (x1, y1), (x2, y2), (255, 0, 0), thickness)
                b = np.array([x1, y1, x2, y2]).astype(int)

                cv2.putText(image, label, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN, 1, (200, 0, 0), 3)
                cv2.putText(image, label, (b[0], b[1] - 10), cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255), 2)
            cv2.imwrite(r"{}/{}".format(path_out, im_name), image)
        logging.info('Your images custom in yolo save in path {}'.format(path_out))
        return self.detections_box
